module gitlab/docker-practice/backend

go 1.13

require (
	github.com/cosmtrek/air v1.12.0 // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/mattn/go-colorable v0.1.6 // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
)
